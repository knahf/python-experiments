#!/usr/bin/python2

import threading

class LockedObject(object):
    """ Wraps __getattribute__ and __setattr__ in a threading.Lock() context manager. Access or modification of
     attributes should be thread safe (i.e. block until other threads are finished accessing the object).

     Intended to be used for storing global state information in threaded scripts.
    """

    _lock = threading.Lock()

    def __getattribute__(self, name):
        # super() is used below to call the default __getattribute__ implementation of the object base class
        #   (this avoids infinite recursion that would break everything).
        with super(LockedObject, self).__getattribute__('_lock'):
            result = super(LockedObject, self).__getattribute__(name)
        return result

    def __setattr__(self, key, value):
        # super() is used below to call the default __setattr__ implementation of the object base class
        #   (this avoids infinite recursion that would break everything).
        with self._lock:

            super(LockedObject, self).__setattr__(key, value)
